FROM gcc:latest

RUN apt-get update -y;\
    apt-get install cpio rsync bc -y; 

WORKDIR /root

RUN wget https://github.com/juce-framework/JUCE/releases/download/6.0.8/juce-6.0.8-linux.zip;\
    unzip ./juce-6.0.8-linux.zip
